/**
 * This function is used to iterate an object recursively
 * @param {Object} object 
 * @param {Function} callback 
 */
const iterateObject = (object, callback) => {
    for (key in object) {
        if (typeof object[key] == "object") {
            iterateObject(object[key], callback);
        } else {
            callback(key, object[key]);
        }
    }
}

/**
 * Function to create card result items from an api call
 * @param {Array} items 
 */
const createResultItems = items => {
    // Remove all current results
    const resultSidebar = document.getElementById("result-sidebar");
    resultSidebar.querySelectorAll(".result-item").forEach(item => {
        item.remove();
    });

    // Create a card for each item/result
    for (const item of items) {
        const card = document.createElement("DIV");
        card.classList.add(
            "card", 
            "result-item", 
            "mb-3"
        );

        const cardBody = document.createElement("DIV");
        cardBody.classList.add("card-body");

        const flex = document.createElement("DIV");
        flex.classList.add("d-flex", "flex-column");

        iterateObject(item, (key, value) => {
            const p = document.createElement("P");
            const newKey = key.split("_").join(" ");
            p.innerHTML = `${newKey}: ${value}`;
            flex.appendChild(p);
        });

        cardBody.appendChild(flex);
        card.appendChild(cardBody);
        resultSidebar.appendChild(card);
    }
}