// The ymaps.ready() function will be called when
// all the API components are loaded and the DOM tree is generated.
ymaps.ready(init);
function init(){
    const latMkad = 55.69105847524208                                 // latitude of MKAD(Moskovskaya Kol'tsevaya Avtomobil'naya Doroga, Russia) from https://www.google.com/maps/place/Moskovskaya+Kol'tsevaya+Avtomobil'naya+Doroga,+Russia/@55.6897841,37.3874766,10.75z/data=!4m13!1m7!3m6!1s0x414ab5de2cf06ca9:0x9c87c2dd8a4feb61!2sMoskovskaya+Kol'tsevaya+Avtomobil'naya+Doroga,+Russia!3b1!8m2!3d55.6909315!4d37.4130217!3m4!1s0x414ab5de2cf06ca9:0x9c87c2dd8a4feb61!8m2!3d55.6909315!4d37.4130217
    const lonMkad = 37.41303242758129                                 // longitude of MKAD(Moskovskaya Kol'tsevaya Avtomobil'naya Doroga, Russia) from https://www.google.com/maps/place/Moskovskaya+Kol'tsevaya+Avtomobil'naya+Doroga,+Russia/@55.6897841,37.3874766,10.75z/data=!4m13!1m7!3m6!1s0x414ab5de2cf06ca9:0x9c87c2dd8a4feb61!2sMoskovskaya+Kol'tsevaya+Avtomobil'naya+Doroga,+Russia!3b1!8m2!3d55.6909315!4d37.4130217!3m4!1s0x414ab5de2cf06ca9:0x9c87c2dd8a4feb61!8m2!3d55.6909315!4d37.4130217
        
    // creating the map, assign to variable called myMap
    var myMap = new ymaps.Map("map", {
        center: [latMkad, lonMkad],
        controls: ['zoomControl'],
        zoom: 10 // zoom level, acceptable values: 0 (entire world) to 19
    });
    
    // collection for geobjects that will used in myMap
    var myCollection = new ymaps.GeoObjectCollection({}, {});

    // autocomplete event for geocode input bar
    var geocode = document.getElementById("geocode");
    geocode.oninput = e => {
        ymaps.suggest(e.target.value)
            .then(items => {
                const autocomplete = document.getElementById('location-autocomplete');
                autocomplete.innerHTML = '';
                autocomplete.style.display = (items.length > 0) ? '' : 'none';
                for (let i = 0; i < items.length; i += 1) {
                    const list = document.createElement('LI');
                    list.innerHTML = items[i].displayName;
                    list.onmousedown = () => {
                        geocode.value = list.innerHTML;
                        autocomplete.innerHTML = '';
                        autocomplete.style.display = 'none';
                    }
                    autocomplete.appendChild(list);
                }
            })
            .catch(err => {
                console.error(err);
            });
    };

    // latLongForm onsubmit eventListener target
    // this method will move the map to new center according to latitude and longitude input from form
    // this method will also add straight line from MKAD to latitude and longitude input
    const latLongForm = document.getElementById('lat-long-form');
    latLongForm.addEventListener("lat-long-result", e => {
        const latitude = e.detail[0];
        const longitude = e.detail[1]; 
        
        // define action that  move the map to target location using callback
        // ref: https://yandex.com/dev/maps/jsapi/doc/2.1/ref/reference/map.action.Single.html
        var myCallback = function(err) {
            if (err) {
                throw err;
            }
        }, 
        myAction = new ymaps.map.action.Single({
            center: [latitude, longitude],
            duration: 1000,
            timingFunction: 'ease-in',
            checkZoomRange: true,
            zoom: 10,
            callback: myCallback
        });
        myMap.action.execute(myAction);

        // Adding straight line from MKAD to destination point
        myCollection.removeAll();
        var myPolyline = new ymaps.GeoObject({
            geometry: {
                type: "LineString",
                coordinates: [
                    [latMkad, lonMkad],
                    [latitude, longitude]
                ]
            }
        });
        myCollection.add(myPolyline);
        myMap.geoObjects.add(myCollection);
    })

    // geoCode onsubmit eventListener target
    // this method will move the map to new center according to latitude and longitude input from form
    // this method will also add straight line from MKAD to latitude and longitude input
    const geocodeForm = document.getElementById("geocode-form");
    geocodeForm.addEventListener("geocode-result", e => {
        latLonArr = e.detail;
        
        // define action that  move the map to target location using callback
        // ref: https://yandex.com/dev/maps/jsapi/doc/2.1/ref/reference/map.action.Single.html
        var myCallback = function(err) {
            if (err) {
                throw err;
            }
        }, 
        myAction = new ymaps.map.action.Single({
            center: [latLonArr[0][0], latLonArr[0][1]], 
            duration: 1000,
            timingFunction: 'ease-in',
            checkZoomRange: true,
            zoom: 10,
            callback: myCallback
        });
        myMap.action.execute(myAction);

        // Adding straight line from MKAD to destination point
        myCollection.removeAll();
        var myPolyline = new ymaps.GeoObject({
            geometry: {
                type: "LineString",
                coordinates: [
                    [latMkad, lonMkad],
                    [latLonArr[0][0], latLonArr[0][1]]
                ]
            }
        });
        myCollection.add(myPolyline);
        myMap.geoObjects.add(myCollection);
    })
}