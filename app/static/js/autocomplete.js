/**
 * Function that clears location search autocomplete
 */
function geocodeFocusOut() {
    const autocomplete = document.getElementById('location-autocomplete');
    autocomplete.innerHTML = '';
    autocomplete.style.display = 'none';
}

geocodeFocusOut();