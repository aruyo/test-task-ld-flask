/**
 * Function that disables every submit button on screen
 */
const disableSubmit = () => {
    document.getElementById("geocode-submit").setAttribute("disabled", true);
    document.getElementById("lat-long-submit").setAttribute("disabled", true);
};

/**
 * Function that enables every submit button on screen
 */
const enableSubmit = () => {
    document.getElementById("geocode-submit").removeAttribute("disabled");
    document.getElementById("lat-long-submit").removeAttribute("disabled");
};

/**
 * Function that enables loading animation on a button
 * @param {Element} button 
 */
const enableSpinner = (button) => {
    button.innerHTML = "";
    const span = document.createElement("SPAN");
    span.classList.add("spinner-border", "spinner-border-sm");
    button.appendChild(span);
};

/**
 * Function that disables loading animation on a button
 * @param {Element} button 
 */
const disableSpinner = (button) => {
    button.innerHTML = "Submit";
};

/**
 * Function that handles geocode form submission
 * @param {Event} e 
 */
const geocodeSubmit = e => {
    e.preventDefault();

    /*
        disable submit and enable loading for geocode button
        each time this form is submitted
    */
    const geocodeButton = document.getElementById('geocode-submit');
    disableSubmit();
    enableSpinner(geocodeButton);

    // get geocode input element
    const geocode = document.getElementById('geocode');

    // call geocode api (calculate distance by address)
    fetch(`/calculate_distance_by_geocode?geocode=${geocode.value}`)
        .then(res => {
            if (res.status !== 200) {
                throw new Error(res.statusText);
            }

            return res.json();
        })
        .then(json => {
            createResultItems(json);

            // Create and dispatch a new event each time results are obtained
            const event = new CustomEvent("geocode-result", {
                bubbles: true,
                cancelable: true,
                composed: false,
                detail: json.map(item => {
                    return [item.result.latitude, item.result.longitude];
                })
            });
            document.getElementById("geocode-form").dispatchEvent(event);
        })
        .catch(err => {
            console.error(err);
        })
        .finally(() => {
            disableSpinner(geocodeButton);
            enableSubmit();
        });
}

/**
 * Function that handles latitude longitude form submission
 * @param {Event} e 
 */
const latLongSubmit = e => {
    e.preventDefault();

    /*
        disable submit and enable loading for latitude longitude 
        button each time this form is submitted
    */
    const latlongButton = document.getElementById('lat-long-submit');
    disableSubmit();
    enableSpinner(latlongButton);

    // get latitude and longitude input element
    const latitude = document.getElementById('latitude');
    const longitude = document.getElementById('longitude');

    // call latitude longitude api (calculate distance by latitude and longitude)
    fetch(`/calculate_distance_by_lat_long?lat=${latitude.value}&long=${longitude.value}`)
        .then(res => {
            if (res.status !== 200) {
                throw new Error(res.statusText);
            }

            return res.json();
        })
        .then(json => {
            const items = [];
            items.push(json);
            createResultItems(items);

            // Create and dispatch a new event each time results are obtained
            const event = new CustomEvent("lat-long-result", {
                bubbles: true,
                cancelable: true,
                composed: false,
                detail: [json.latitude, json.longitude]
            });
            document.getElementById("lat-long-form").dispatchEvent(event);
        })
        .catch(err => {
            console.error(err);
        })
        .finally(() => {
            disableSpinner(latlongButton);
            enableSubmit();
        });    
}