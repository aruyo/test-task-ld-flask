from flask import Flask
from math import sin, cos, sqrt, atan2, radians
import requests
import logging
import os

from dotenv import load_dotenv
load_dotenv()

app = Flask(__name__)
class MapsController():
    def __init__(self):
        pass
    
    #function to calculate distance between 2 latitude and longitude
    #by default origin latitude and longitude is set to MKAD (Moskovskaya Kol'tsevaya Avtomobil'naya Doroga, Russia)
    #return type dictionary
    #with format : {distance}  in KM
    def calculateDistanceByLatLng(lat, lng):
        response = {}                                                     #initial response dictionary

        #span of MKAD in longitude and latitude. Reference: https://en.wikipedia.org/wiki/Module:Location_map/data/Russia_Moscow_MKAD
        latTopMKAD = 55.917
        latBotMKAD = 55.503
        longRightMKAD = 37.895
        longLeftMKAD = 37.329

        #check whether the point is inside MKAD or not
        if (lat < latTopMKAD) and (lat > latBotMKAD) and (lng < longRightMKAD) and (lng > longLeftMKAD):
            response["distance"] = "Location is inside the MKAD"
        else:
            # approximate radius of earth in km
            R = 6373.0
            
            log = {}                                                          
            lat1 = radians(55.69105847524208)                                 #latitude of MKAD(Moskovskaya Kol'tsevaya Avtomobil'naya Doroga, Russia) from https://www.google.com/maps/place/Moskovskaya+Kol'tsevaya+Avtomobil'naya+Doroga,+Russia/@55.6897841,37.3874766,10.75z/data=!4m13!1m7!3m6!1s0x414ab5de2cf06ca9:0x9c87c2dd8a4feb61!2sMoskovskaya+Kol'tsevaya+Avtomobil'naya+Doroga,+Russia!3b1!8m2!3d55.6909315!4d37.4130217!3m4!1s0x414ab5de2cf06ca9:0x9c87c2dd8a4feb61!8m2!3d55.6909315!4d37.4130217
            lon1 = radians(37.41303242758129)                                 #longitude of MKAD(Moskovskaya Kol'tsevaya Avtomobil'naya Doroga, Russia) from https://www.google.com/maps/place/Moskovskaya+Kol'tsevaya+Avtomobil'naya+Doroga,+Russia/@55.6897841,37.3874766,10.75z/data=!4m13!1m7!3m6!1s0x414ab5de2cf06ca9:0x9c87c2dd8a4feb61!2sMoskovskaya+Kol'tsevaya+Avtomobil'naya+Doroga,+Russia!3b1!8m2!3d55.6909315!4d37.4130217!3m4!1s0x414ab5de2cf06ca9:0x9c87c2dd8a4feb61!8m2!3d55.6909315!4d37.4130217
            lat2 = radians(lat)
            lon2 = radians(lng)

            dlon = lon2 - lon1                                                #calculate distance reference https://stackoverflow.com/questions/19412462/getting-distance-between-two-points-based-on-latitude-longitude
            dlat = lat2 - lat1                                                #calculate distance reference https://stackoverflow.com/questions/19412462/getting-distance-between-two-points-based-on-latitude-longitude

            a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2   #calculate distance reference https://stackoverflow.com/questions/19412462/getting-distance-between-two-points-based-on-latitude-longitude
            c = 2 * atan2(sqrt(a), sqrt(1 - a))                               #calculate distance reference https://stackoverflow.com/questions/19412462/getting-distance-between-two-points-based-on-latitude-longitude

            distance = R * c                                                  #calculate distance reference https://stackoverflow.com/questions/19412462/getting-distance-between-two-points-based-on-latitude-longitude

            response["distance"] = str(distance)                              #set distance to dictionary response 

        response["distance_unit"] = "KM"                                  #set distance unit to dictionary response
        response["latitude"] = lat                                        #set latitude of the location to dictionary response
        response['longitude'] = lng                                       #set longitude of the location to dictionary response
        return response

    #function to calculate distance from by request
    #by default origin is set to MKAD (Moskovskaya Kol'tsevaya Avtomobil'naya Doroga, Russia)
    #return type dictionary
    #with format : {origin}
    def calculateDistanceByGeoCode(request):

        url = "https://geocode-maps.yandex.ru/1.x/?format=json&apikey="+ os.environ.get('YANDEX_API_KEY') +"&geocode="+request+"&lang=en-US"    #url get data by geocode to yandex maps api
        response = []                                                                                                                           #init response
        parsedData = []                                                                                                                         #init parsedData
        log = {}                                                                                                                                #init log 
        response=requests.get(url).json()                                                                                                       #get data by geocode to given url
        for i in response['response']['GeoObjectCollection']['featureMember']:                                                                  #loop response
             result = {}                                                                                                                        #set result to empty
             position = i['GeoObject']['Point']['pos']                                                                                          #get the position
             splitPos = position.split(" ")                                                                                                     #split
             lat = splitPos[1]                                                                                                                  #get latitude from split position
             lng = splitPos[0]                                                                                                                  #get longitude from split position
             result['name'] = i['GeoObject']['name']                                                                                            #get getObject name
             result['result'] = MapsController.calculateDistanceByLatLng(float(lat), float(lng))                                                #calculate with latitude and longitude
             parsedData.append(result)                                                                                                          #append to parsedData
             logging.basicConfig(filename='distances.log', level=logging.DEBUG,format='%(asctime)s : %(message)s')                              #formatting log
             log['from'] = "MKAD"
             log['to'] = result['name']
             log['distance'] = result['result']
             app.logger.info(log)                                                                                                               #create log                                                                                                                                                                                                                               

        return parsedData

mapsController = MapsController()