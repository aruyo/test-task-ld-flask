from app.controllers.MapsController import MapsController
from app import app
from flask import request, jsonify, render_template
import os

#function index
#to load view page
#return view index.html with api_key get from environment variable
@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html', api_key=os.environ.get('YANDEX_API_KEY'))

#function calculateLatLong
#to receive request with lat and long parameter
#return with json format
@app.route('/calculate_distance_by_lat_long')
def calculateLatLong():
    latitude = request.args.get('lat')
    longitude = request.args.get('long')
    return jsonify(MapsController.calculateDistanceByLatLng(float(latitude), float(longitude)))

#function calculateDistanceByGeoCode
#to receive request with geocode
#return with json format
@app.route('/calculate_distance_by_geocode')
def calculateDistanceByGeoCode():
    geocode = request.args.get('geocode')
    return jsonify(MapsController.calculateDistanceByGeoCode(geocode))
