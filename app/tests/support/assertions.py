import json
from os.path import join, dirname
from jsonschema import validate

#function to check schema wether the given data with the json file were matches
#return boolean (true or false)
def assert_valid_schema(data, schema_file):
    """ Checks whether the given data matches the schema """

    schema = _load_json_schema(schema_file)                     #call load json schema
    return validate(data, schema)                               #validate from jsonschema and return boolean

#function to load json schema
#return json
def _load_json_schema(filename):
    """ Loads the given schema file """

    relative_path = join('schemas', filename)                  #get schemas folder path
    absolute_path = join(dirname(__file__), relative_path)     #get schemas folder and file path

    with open(absolute_path) as schema_file:                   #open the path
        return json.loads(schema_file.read())                  #and return as json