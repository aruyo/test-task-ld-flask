import json
from .support.assertions import assert_valid_schema
import os

#function to test position in MKAD
#assert return must be have string 'Locations is inside the MKAD'
def test_check_wether_position_in_mkad(app, client):
    res = client.get('/calculate_distance_by_lat_long?lat=55.689654&long=37.448563')                    #call calculate distance by latitude and longitude with lat and long not far from MKAD
    assert res.status_code == 200                                                                       #assert status code to 200
    expected = {                                                                                        #set expected output
        'distance': 'Location is inside the MKAD',
        'distance_unit': 'KM',
        'latitude': 55.689654,
        'longitude': 37.448563
    }
    assert expected == json.loads(res.get_data(as_text=True))                                           #compare expected result with result from call endpoint or url

#function to test calculate position by lat and long
#assert return must be distance from MKAD to latitude and longitude destination
def test_calculate_position_by_lat_lng(app, client):
    #lat lng that use for sample is HillPark
    res = client.get('/calculate_distance_by_lat_long?lat=52.76353280510108&long=39.667099302270096')   #call calculate distance by latitude and longitude with lat and long set to HillPark
    assert res.status_code == 200                                                                       #assert status code to 200
    expected = {
        'distance': '357.04486941736656',                                                               #set expected output
        'distance_unit': 'KM',
        'latitude': 52.76353280510108,
        'longitude': 39.667099302270096
    }
    assert expected == json.loads(res.get_data(as_text=True))                                           #compare expected result with result from call endpoint or url

#function to test calculate position by geocode
#assert return must be have valid schema with json structure at support/schemas/geocode_schema.json
def test_calculate_position_by_geocode(app, client):
    res = client.get('/calculate_distance_by_geocode?geocode=MKAD')                                     #call calculate distance by geocode and set it to MKAD          
    assert res.status_code == 200                                                                       #assert status code to 200
    assert_valid_schema(json.loads(res.get_data(as_text=True)), 'geocode_schema.json')                  #validate expected json schema with result from call endpoint or url

#function to test logging to file
#assert return must check the log file exist
def test_logging_to_file(app, client):
    res = client.get('/calculate_distance_by_geocode?geocode=MKAD')                                     #call calculate distance by geocode and set it to MKAD 
    assert res.status_code == 200                                                                       #assert status code to 200
    assert os.path.exists(os.path.join(os.path.abspath(os.curdir),"distances.log")) == 1                #check distances.log is exist
