import pytest

from app import app as flask_app

#Define fixtures for static data used by tests. 
#This data can be accessed by all tests in the suite unless specified otherwise.
@pytest.fixture
def app():
    yield flask_app             #define flask app

#Define fixtures for static data used by tests. 
#This data can be accessed by all tests in the suite unless specified otherwise.
@pytest.fixture
def client(app):
    return app.test_client()    #define app test client